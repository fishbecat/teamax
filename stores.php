<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>喫茶享樂  Enjoy Tea</title>
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<!--layerslider-->
<link href="js/layerslider/css/layerslider.css" rel="stylesheet" type="text/css">
<!--layersliderEnd-->
</head>

<body id="branch_stores">
<?php
    require ('FrontClass/Base.php');
    require ('FrontClass/Stores.php');
    $stores = new Stores();
?>
<!--[if lte IE 8]>
<div class="chromeframe">您正在使用 <strong>非常古老</strong> 的瀏覽器！強烈的建議 <a href="http://browsehappy.com/" target="_blank">更新您的瀏覽器</a> ，來改善您的上網體驗。</div>
<![endif]-->
<div id="header">
  <div id="headerInner"> <a href="index.html"><img src="img/logo1.png" name="logo" width="301" height="94" id="logo" /></a> <img class="title" src="img/STORES.png" width="157" height="116"  alt=""/>
    <ul id="nav">
      <li id="navAbout"><a href="about.html" id="about">關於喫茶</a> <span id="inAbout"><img class="btnLeaf" src="img/leaf.png" width="31" height="22"  alt=""/><a href="about.html">品牌精神</a><a href="tactics.html">經營策略</a></span> </li>
      <li id="navNews"><a href="news.php" id="news">活動消息</a> <span id="inNews"><img class="btnLeaf" src="img/leaf.png" width="31" height="22"  alt=""/><a href="news.php">最新消息</a><a href="report.php">推薦報導</a></span> </li>
      <li id="navProducts"><a href="hot_drinks.html" id="products">飲品介紹</a> <span id="inProducts"><img class="btnLeaf" src="img/leaf.png" width="31" height="22"  alt=""/><a href="hot_drinks.html">人氣飲品</a><a href="menu.html">MENU</a></span></li>
      <li><a href="stores.php" id="branches">喫茶享樂去</a></li>
      <li id="navJoin"><a href="joinus.html" id="join">加盟專區</a> <span id="inJoin"><img class="btnLeaf1" src="img/leaf.png" width="31" height="22"  alt=""/><a href="joinus.html">加盟訊息</a><a href="joinus_process.html">加盟流程</a><a href="condition.html">加盟條件</a></span></li>
    </ul>
  </div>
</div>
<div id="bannerIn">
  <div id="bannerInner">
    <!--Banner動畫-->
    <div id="layerslider" style="width: 960px; height: 270px; position:relative; z-index:1;">
      <div class="ls-slide" data-ls="slidedelay: 4000; transition2d: 2,7,9;"> <img class="ls-l" style="position:absolute;top:25px;left:170px;" data-ls="offsetxin:50;durationin:750;easingin:easeOutBack;skewxin:-50;offsetxout:-50;" src="img/bannerBS_01.jpg" width="194" height="81" alt=""/> <img class="ls-l" style="position:absolute;top:115px;left:220px;" src="img/bannerBS_02.jpg" width="98" height="26" alt=""/> <img class="ls-l" style="position:absolute;top:105px;left:400px;" data-ls="offsetxin:0;durationin:2000;delayin:1500;easingin:easeOutElastic;rotatexin:-90;transformoriginin:50% top 0;offsetxout:-200;durationout:1000;" src="img/bannerBS_03.jpg" width="191" height="93" alt=""/> <img class="ls-l" style="position:absolute;top:195px;left:350px;" data-ls="offsetxin:0;durationin:2000;delayin:2000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% top 0;offsetxout:-400;" src="img/bannerBS_04.jpg" width="298" height="32" alt=""/> <img class="ls-l" style="position:absolute;top:20px;left:650px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:100;durationin:2000;delayin:1000;" src="img/bannerBS_Co.png" width="213" height="250" alt=""> <img class="ls-l" style="position:absolute;top:46px;left:494px;white-space: nowrap;" data-ls="offsetxin:0;durationin:3000;delayin:500;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;" src="img/bannerBS_Gr.png" width="60" height="59" alt=""/> <img class="ls-l" style="position:absolute;top:55px;left:106px;white-space: nowrap;" data-ls="offsetxin:-100;offsetyin:-200;durationin:2000;delayin:750;easingin:easeInOutQuad;rotatein:90;"  src="img/bannerBS_Or.png" width="76" height="77" alt=""> </div>
      <ul id="subnavBS">
        <li><a href="stores.php" id="stores">門市資訊</a></li>
      </ul>
    </div>
    <!--Banner動畫end-->
  </div>
</div>
<div id="content">
  <div id="contentInnerBS">
    <!--右上列 首頁及聯絡我們-->
    <ul id="sidebar">
      <li><a href="index.html">首頁</a></li>
      <li><a href="contact.html">聯絡我們</a></li>
    </ul>
    <!--右上列 首頁及聯絡我們end-->
    <div id="search">
      <ul id="map_store">
        <li class="location" id="north">北區</li>
        <li class="location" id="mid">中區 </li>
        <li class="location" id="south">南區</li>
        <li class="location" id="east">東區</li>
        <hr class="hr_line"/>
      </ul>
        <?php
        $map = array(
            'north' => 'n_ta',
            'mid' => 'mid_ta',
            'south' => 's_ta',
            'east' => 'e_ta',
        );

        foreach ($map as $loc => $value) {
            echo "<div id='{$loc}_city'><img id='map_{$loc}' src='img/map_{$loc}.png' width='167' height='229' alt=''/>";
            echo "<table width='247' border='0' class='$value'>";

            $count = 0;
            $store = $stores->getLocation($loc);
            foreach ($store as $key => $value) {
                if ($count % 3 === 0) {
                    echo "<tr>";
                }

                echo "<td width='20'><img src='img/storeLeaf_{$value['stat']}.png' width='16' height='11'  alt=''/></td>";
                echo "<td width='85' align='left' class='location' id='{$key}'>{$value['name']}</td>";

                ++$count;

                if ($count % 3 === 0 || $count === count($store)) {
                    echo "</tr>";
                }
            }

            echo "</table>";
            echo "</div>";
        }
        ?>
      <img id="search_Title"src="img/search.png" width="158" height="17"  alt=""/>
      <img id="injoytea" src="img/injoytea.png" width="141" height="244"  alt=""/>
      <div id="no_tab">
        <table width="600" border="0" class="store_info" id="">
          <tr>
            <th width="140" scope="col">店面</th>
            <th width="110" scope="col">電話</th>
            <th width="270" scope="col">地址</th>
            <th width="80" scope="col">地圖</th>
          </tr>
          <tr>
            <td>無</td>
            <td>無</td>
            <td>無</td>
            <td><img src="img/arrowhead_bottom.png" width="9" height="7"  alt=""/></td>
          </tr>
        </table>
      </div>
      <div id="show_tab"></div>
      <div id="page_tab"></div>
    </div>
  </div>
</div>
<div id="footer">
  <div id="footerInner">
    <a href="index.html"><img src="img/logo.png" name="logo" width="240" height="100" id="footerLogo" /></a>
    <ul>
      <li><a href="about.html">關於喫茶</a></li>
      <li><a href="news.php">活動消息</a></li>
      <li><a href="hot_drinks.html">飲品介紹</a></li>
      <li><a href="stores.php">分店資訊</a></li>
      <li><a href="joinus.html">加盟專區</a></li>
      <li><a href="contact.html">聯絡我們</a></li>
      <li><a href="https://www.facebook.com/pages/%E8%94%A1%E7%B1%83%E5%AD%90/788019037902736" target="_blank"><img src="img/FB.png" width="14" height="14"  alt=""></a></li>
    </ul>
    <p id="address">地址：台中市十甲路216號 ‧ 服務專線：04-22135886 ‧ 加盟專線：0989-111528 陳先生</p>
    <p id="copyright">© 2013 喫茶享樂 All Rights Reserved. Designed by <a href="http://fishsaut.com/" title="魚躍創意．品牌．設計．網頁" target="_blank">魚躍創意</a></p>
  </div>
</div>

<script src="js/jquery-1.10.2.min.js"></script>
<!--layerslider-->
<script src="js/layerslider/js/greensock.js"></script>
<script src="js/layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
<script src="js/layerslider/js/layerslider.transitions.js"></script>
<!--layersliderEnd-->
<script>
$(document).ready(function(){
    <!--layerslider-->
   $('#layerslider').layerSlider({});
       <!--layersliderEnd-->
 <!--content動畫-->
   $(window).scroll(function(){
    if ($(this).scrollTop() > 39) {
        $("#teaman").stop().animate({opacity:1},1000);
        $("#injoytea").stop().animate({opacity:1},1000);
    }
});

<!--content動畫end-->
  $("#navAbout").hover(function(){
    $("#inAbout").stop().fadeIn(500);
 },function(){
    $("#inAbout").hide();
 });
  $("#navNews").hover(function(){
    $("#inNews").stop().fadeIn(500);
 },function(){
    $("#inNews").hide();
 });
   $("#navProducts").hover(function(){
    $("#inProducts").stop().fadeIn(500);
 },function(){
    $("#inProducts").hide();
 });
    $("#navJoin").hover(function(){
    $("#inJoin").stop().fadeIn(500);
 },function(){
    $("#inJoin").hide();
 });
});

<!--預設選擇中部-->
showData("mid", 1);

<!--分店選單-->
$(".location").on("click", function(event) {
    showData(this.id, 1);
});

function showData(id, page) {
    if (id === 'north') {
        $('#north_city').show();
        $('#mid_city').hide();
        $('#south_city').hide();
        $('#east_city').hide();
    }

    if (id === 'mid') {
        $('#north_city').hide();
        $('#mid_city').show();
        $('#south_city').hide();
        $('#east_city').hide();
    }

    if (id === 'south') {
        $('#north_city').hide();
        $('#mid_city').hide();
        $('#south_city').show();
        $('#east_city').hide();
    }

    if (id === 'east') {
        $('#north_city').hide();
        $('#mid_city').hide();
        $('#south_city').hide();
        $('#east_city').show();
    }

    $('#show_tab').empty();
    $('#page_tab').empty();

    var form_data = new FormData();

    form_data.append("location", id);
    form_data.append("page", page);
    form_data.append("type", "getStores");

    var request = $.ajax({
        url: "./FrontClass/Stores.php",
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        method: "post"
    });

    request.success(function(msg) {
        var data = JSON.parse(msg);

        if (data['result'] !== 'ok') {
            alert(data['msg']);
            return ;
        }

        if (!data['ret'].length) {
            $("#no_tab").show();
            $("#show_tab").hide();
        } else {
            $("#no_tab").hide();
            $("#show_tab").show();

            var ret = "<table width='600' border='0' class='store_info'>";
            ret += "<tr>";
            ret += "<th width='140' scope='col'>店面</th>";
            ret += "<th width='110' scope='col'>電話</th>";
            ret += "<th width='270' scope='col'>地址</th>";
            ret += "<th width='80' scope='col'>地圖</th>";
            ret += "</tr>";

            for (var key in data['ret']) {
                ret += "<tr>";
                ret += "<td>" + data['ret'][key]['name'] + "</td>";
                ret += "<td>" + data['ret'][key]['telephone'] + "</td>";
                ret += "<td>" + data['ret'][key]['address'] + "</td>";
                ret += "<td><img src='img/arrowhead_bottom.png' width='9' height='7'  alt=''/></td>";
                ret += "</tr>";
            }
            ret += "</table>";
            ret += "<input type='hidden' name='location_id' value='" + id + "'>";

            var pageRet = "";
            if (data['count'] !== 1) {
                pageRet += "<table width='100%' border='0' class='btn_page'>";
                pageRet += "<tbody>";
                pageRet += "<tr>";

                for (var i = 1; i <= data['count']; ++i) {
                    pageRet += "<td align='center' class='page_number'>" + i + "</td>";
                }

                pageRet += "</tr>";
                pageRet += "<tr>";
                pageRet += "<td colspan='7'><br></td>";
                pageRet += "</tr>";
                pageRet += "</tbody>";
                pageRet += "</table>";
            }

            $('#show_tab').html(ret);
            $('#page_tab').html(pageRet);

            $(".page_number").on("click", function(event) {
                showData($('input[name="location_id"]').val(), this.innerText);
            });

            $(".store_info tr td:nth-child(3)").each(function(){
                var location = $(this).text();
                $(this).next().html('<a href="https://www.google.com.tw/maps/place/'+location+'" target="_blank"><img src="img/arrowhead_bottom.png" width="9" height="7" alt=""/></a>');
            });
        }
    });

    request.error(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}
</script>
</body>
</html>