<?php

/**
 * 最新消息
 */
class News extends Base
{
    /**
     * 回傳最新飲品
     *
     * @return array
     */
    public function getNewDrink() {
        $query = $this->conn->query('SELECT * FROM new_drink ORDER BY id');

        return $query->fetch_all(MYSQLI_ASSOC);
    }

    /**
     * 回傳最新店面
     *
     * @return array
     */
    public function getNewStores() {
        $query = $this->conn->query("SELECT * FROM new_stores ORDER BY begin DESC");
        $result = $query->fetch_all(MYSQLI_ASSOC);

        return $result;
    }
}
