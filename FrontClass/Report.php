<?php

/**
 * 店面資料
 */
class Report extends Base
{
    /**
     * 回傳店面資料
     *
     * @return array
     */
    public function getReport() {
        $query = $this->conn->query("SELECT * FROM media_report ORDER BY begin ASC");
        $result = $query->fetch_all(MYSQLI_ASSOC);

        return $result;
    }
}
