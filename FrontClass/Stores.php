<?php

require_once 'Base.php';

/**
 * 店面資料
 */
class Stores extends Base
{
    /**
     * 區域對應
     *
     * @var array
     */
    private $areaMap = array(
        'north' => array(1, 2, 3, 4, 5),
        'mid' => array(6, 7, 8, 9, 10),
        'south' => array(11, 12, 13, 14),
        'east' => array(15, 16, 17, 18),
        'Keelung' => array(1), // 基隆市
        'Taipei' => array(2), // 台北市
        'NewTaipei' => array(3), // 新北市
        'Taoyuan' => array(4), // 桃園市
        'Hsinchu' => array(5), // 新竹縣市
        'Miaoli' => array(6), // 苗栗縣
        'Taichung' => array(7), // 台中市
        'Changhua' => array(8), // 彰化縣
        'Nantou' => array(9), // 南投縣
        'Yunlin' => array(10), // 雲林縣
        'Chiayi' => array(11), // 嘉義縣市
        'Tainan' => array(12), // 台南市
        'Kaohsiung' => array(13), // 高雄市
        'Pingtung' => array(14), // 屏東縣
        'Yilan' => array(15), // 宜蘭縣
        'Hualien' => array(16), // 花蓮縣
        'Taitung' => array(17), // 台東縣
        'Outer' => array(18), // 外島
    );

    /**
     * 地點
     *
     * @var array
     */
    private $locationMap = array(
        1 => '基隆市',
        2 => '台北市',
        3 => '新北市',
        4 => '桃園市',
        5 => '新竹縣市',
        6 => '苗栗縣',
        7 => '台中市',
        8 => '彰化縣',
        9 => '南投縣',
        10 => '雲林縣',
        11 => '嘉義縣市',
        12 => '台南市',
        13 => '高雄市',
        14 => '屏東縣',
        15 => '宜蘭縣',
        16 => '花蓮縣',
        17 => '台東縣',
        18 => '外島',
    );

    /**
     * 回傳店面資料
     *
     * @param string $area 區域
     * @param string $page 分頁
     * @return array
     */
    public function getStores($area, $page) {
        if (!isset($this->areaMap[$area])) {
            return json_encode(array('result' => 'error', 'msg' => '地點錯誤'));
        }

        $data = $this->conn->real_escape_string(implode(', ', $this->areaMap[$area]));
        $sql = "SELECT * FROM stores WHERE location IN ($data) ORDER BY location LIMIT $page, 12";
        $query = $this->conn->query($sql);
        $result = $query->fetch_all(MYSQLI_ASSOC);

        $sqlCount = "SELECT * FROM stores WHERE location IN ($data)";
        $queryCount = $this->conn->query($sqlCount);
        $count = $queryCount->num_rows;

        $ret = array(
            'result' => 'ok',
            'count' => ceil($count / 12),
            'ret' => $result
        );
        return json_encode($ret);
    }

    /**
     * 回傳店面資料
     *
     * @param string $area 區域
     * @return array
     */
    public function getLocation($area) {
        if (!isset($this->areaMap[$area])) {
            return json_encode(array('result' => 'error', 'msg' => '地點錯誤'));
        }

        $data = $this->conn->real_escape_string(implode(', ', $this->areaMap[$area]));
        $sql = "SELECT location FROM stores WHERE location IN ($data) GROUP BY location";
        $query = $this->conn->query($sql);

        $hasData = array();
        foreach ($query->fetch_all(MYSQLI_ASSOC) as $value) {
            $hasData[] = $value['location'];
        }

        $ret = array();
        foreach ($this->areaMap[$area] as $value) {
            $stat = 0;

            if (in_array($value, $hasData)) {
                $stat = 1;
            }

            $key = array_search(array($value), $this->areaMap);
            $ret[$key]['name'] = $this->locationMap[$value];
            $ret[$key]['stat'] = $stat;
        }

        return $ret;
    }
}

$location = filter_input(INPUT_POST, 'location');
$page = filter_input(INPUT_POST, 'page');
$type = filter_input(INPUT_POST, 'type');

// 回傳店面資料API
if ($type == 'getStores') {
    if (!$location) {
        $location = 'mid';
    }

    if (!$page) {
        $page = 1;
    }

    $stores = new Stores();
    echo $stores->getStores($location, ($page - 1) * 12);
}