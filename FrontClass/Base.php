<?php

/**
 * 共用類別
 */
class Base {
    /**
     * DB server
     *
     * @var string
     */
    private $servername = 'localhost';

    /**
     * DB username
     *
     * @var string
     */
    private $username = 'fishsaut_fish';

    /**
     * DB password
     *
     * @var string
     */
    private $password = 'Fishsaut.com.0524';

    /**
     * DB name
     *
     * @var type
     */
    private $dbname = 'fishsaut_teaMax';

    /**
     * mysqli connection
     *
     * @var mysqli
     */
    public $conn;

    /**
     * 初始化
     */
    public function __construct() {
        $conn = new mysqli(
            $this->servername,
            $this->username,
            $this->password,
            $this->dbname
        );

        // Check connection
        if ($conn->connect_error) {
            throw new \InvalidArgumentException('資料庫讀取失敗');
        }

        $this->conn = $conn;
    }
}
