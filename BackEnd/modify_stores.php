<?php

require ('BackClass/Base.php');
require ('BackClass/Stores.php');

try {
    $stores = new Stores();
    $ret = array('result' => 'ok');

    // 取得會用到的參數
    $type = filter_input(INPUT_POST, 'type');
    $id = filter_input(INPUT_POST, 'id');
    $name = filter_input(INPUT_POST, 'name');
    $location = filter_input(INPUT_POST, 'location');
    $address = filter_input(INPUT_POST, 'address');
    $telephone = filter_input(INPUT_POST, 'telephone');

    if (!$type) {
        throw new \InvalidArgumentException('未指定類型');
    }

    // 新增
    if ($type == 'insert') {
        if (!$name) {
            throw new \InvalidArgumentException('未指定店名');
        }

        if (!$location) {
            throw new \InvalidArgumentException('未指定縣市');
        }

        if (!$address) {
            throw new \InvalidArgumentException('未指定地址');
        }

        if (!$telephone) {
            throw new \InvalidArgumentException('未指定電話');
        }

        $options = array(
            'name' => $name,
            'location' => $location,
            'address' => $address,
            'telephone' => $telephone,
        );

        $stores->insert($options);
    }

    // 修改
    if ($type == 'edit') {
        if (!$id) {
            throw new \InvalidArgumentException('未指定id');
        }

        if (!$name) {
            throw new \InvalidArgumentException('未指定標題');
        }

        if (!$location) {
            throw new \InvalidArgumentException('未指定縣市');
        }

        if (!$address) {
            throw new \InvalidArgumentException('未指定地址');
        }

        if (!$telephone) {
            throw new \InvalidArgumentException('未指定電話');
        }

        $options = array(
            'id' => $id,
            'name' => $name,
            'location' => $location,
            'address' => $address,
            'telephone' => $telephone,
        );

        $stores->edit($options);
    }

    // 刪除
    if ($type == 'delete') {
        if (!$id) {
            throw new \InvalidArgumentException('未指定id');
        }

        $stores->delete($id);
    }

    echo json_encode($ret);
} catch (\Exception $e) {
    $output = array(
        'result' => 'error',
        'msg' => $e->getMessage()
    );

    echo json_encode($output);
}
