<fieldset>
<legend>新增資料</legend>
<div class="new_stores_insert">
    店名: <input type="text" name="name"><br>
    創始日期: <?php echo $newStores->getBeginSelectBar(); ?><br>
    地址: <input type="text" name="address"><br>
    圖片: <input type="file" name="file"><br>
    <input type="button" class="insert" value="新增"><br>
</div>
</fieldset>

<div class="new_stores_content">
<?php

$array = $newStores->show();

foreach ($array as $value) {
    echo "<span class='left'>";
    echo "店名: <input type='text' name='name' value='$value[name]'><br>";
    echo "創始日期: " . $newStores->getBeginSelectBar($value['begin']) . "<br>";
    echo "地址: <input type='text' name='address' value='$value[address]'><br>";
    echo "圖片: <input type='file' name='file'><br>";
    echo "<input type='button' class='edit' value='修改'>&nbsp;";
    echo "<input type='button' class='delete' value='刪除'><br>";
    echo "<input type='hidden' name='id' value='$value[id]'>";
    echo "<input type='hidden' name='filename' value='$value[filename]'>";
    echo "</span>";

    echo "<span class='right'>";
    echo "<img src='../img/$value[filename]'>";
    echo "</span>";
}
?>
</div>

<script>
var url = 'index.php?mode=new_stores';
var now = new Date();

$('.insert').on('click', function(event) {
    var target = $(event.target).parent();
    var form_data = new FormData();
    var year = target.find("select[name=year]").val() * 10000;
    var month = target.find("select[name=month]").val() * 100;
    var day = target.find("select[name=day]").val() * 1;

    form_data.append('type', 'insert');
    form_data.append('name', target.find("input[name=name]").val());
    form_data.append('begin', year + month + day);
    form_data.append('address', target.find("input[name=address]").val());
    form_data.append('file', target.find("input[name=file]").prop("files")[0]);

    var request = $.ajax({
        url: './modify_new_stores.php',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        method: 'post'
    });

    request.success(function(msg) {
        var data = JSON.parse(msg);
        if (data.result === 'error') {
            alert(data.msg);
        } else {
            window.location = url + '&rd=' + now.getMilliseconds();
        }
    });

    request.error(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
});

$('.edit').click(function() {
    var target = $(event.target).parent();
    var form_data = new FormData();
    var year = target.find("select[name=year]").val() * 10000;
    var month = target.find("select[name=month]").val() * 100;
    var day = target.find("select[name=day]").val() * 1;

    form_data.append('type', 'edit');
    form_data.append('id', target.find("input[name=id]").val());
    form_data.append('name', target.find("input[name=name]").val());
    form_data.append('begin', year + month + day);
    form_data.append('address', target.find("input[name=address]").val());
    form_data.append('file', target.find("input[name=file]").prop("files")[0]);
    form_data.append('filename', target.find("input[name=filename]").val());

    var request = $.ajax({
        url: './modify_new_stores.php',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        method: 'post'
    });

    request.success(function(msg) {
        var data = JSON.parse(msg);
        if (data.result === 'error') {
            alert(data.msg);
        } else {
            window.location = url + '&rd=' + now.getMilliseconds();
        }
    });

    request.error(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
});

$('.delete').click(function() {
    var target = $(event.target).parent();
    var form_data = new FormData();

    form_data.append('type', 'delete');
    form_data.append('id', target.find("input[name=id]").val());
    form_data.append('filename', target.find("input[name=filename]").val());

    var request = $.ajax({
        url: './modify_new_stores.php',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        method: 'post'
    });

    request.success(function(msg) {
        var data = JSON.parse(msg);
        if (data.result === 'error') {
            alert(data.msg);
        } else {
            window.location = url + '&rd=' + now.getMilliseconds();
        }
    });

    request.error(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
});
</script>