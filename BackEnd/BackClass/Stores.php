<?php

/**
 * 店面資料
 */
class Stores extends Base
{
    /**
     * 區域
     *
     * @var array
     */
    private $areaMap = array(
        '北部' => array(1, 2, 3, 4, 5),
        '中部' => array(6, 7, 8, 9, 10),
        '南部' => array(11, 12, 13, 14),
        '東部' => array(15, 16, 17, 18),
    );

    /**
     * 地點
     *
     * @var array
     */
    private $locationMap = array(
        1 => '基隆市',
        2 => '台北市',
        3 => '新北市',
        4 => '桃園市',
        5 => '新竹縣市',
        6 => '苗栗縣',
        7 => '台中市',
        8 => '彰化縣',
        9 => '南投縣',
        10 => '雲林縣',
        11 => '嘉義縣市',
        12 => '台南市',
        13 => '高雄市',
        14 => '屏東縣',
        15 => '宜蘭縣',
        16 => '花蓮縣',
        17 => '台東縣',
        18 => '外島',
    );

    /**
     * 回傳店面資料
     *
     * @param string $location 地點
     * @return array
     */
    public function show($location) {
        if (isset($this->locationMap[$location])) {
            $data = array($location);
        } else {
            return json_encode(array('result' => 'error', 'msg' => '地點錯誤'));
        }

        $array = $this->conn->real_escape_string(implode(', ', $data));
        $sql = "SELECT * FROM stores WHERE location IN ($array) ORDER BY location";
        $query = $this->conn->query($sql);
        $result = $query->fetch_all(MYSQLI_ASSOC);

        return $result;
    }

    /**
     * 新增店面資料
     *
     * @param array $options 包含內容如下
     *     name string 名稱
     *     location string 地點
     *     address string 地址
     *     telephone string 電話
     */
    public function insert($options) {
        $area = $this->searchArea($options['location']);

        $stmt = $this->conn->prepare(
            'INSERT INTO stores (name, area, location, address, telephone) '.
            'VALUES (?, ?, ?, ?, ?)'
        );

        $stmt->bind_param(
            'sssss',
            $options['name'],
            $area,
            $options['location'],
            $options['address'],
            $options['telephone']
        );

        $stmt->execute();
    }

    /**
     * 修改店面資料
     *
     * @param array $options 包含內容如下
     *     id integer 店面資料id
     *     name string 名稱
     *     location string 地點
     *     address string 地址
     *     telephone string 電話
     */
    public function edit($options) {
        $area = $this->searchArea($options['location']);

        $stmt = $this->conn->prepare(
            'UPDATE stores SET name = ?, area = ?, location = ?, '.
            'address = ?, telephone = ? WHERE id = ?'
        );

        $stmt->bind_param(
            'sssssi',
            $options['name'],
            $area,
            $options['location'],
            $options['address'],
            $options['telephone'],
            $options['id']
        );

        $stmt->execute();
    }

    /**
     * 刪除店面資料
     *
     * @param integer $id 店面資料id
     */
    public function delete($id) {
        $stmt = $this->conn->prepare(
            'DELETE FROM stores WHERE id = ?'
        );

        $stmt->bind_param('i', $id);
        $stmt->execute();
    }

    /**
     * 取得地點select bar
     *
     * @param string $location 地點
     * @return string
     */
    public function getLocationSelectBar($location, $status = false) {
        $str = "<select name='location'>";

        if ($status) {
            $str = "<select name='location' id='location_bar'>";
        }

        foreach ($this->locationMap as $key => $value) {
            if ($key == $location) {
                $str .= "<option value='$key' selected>$value</option>";

                continue;
            }

            $str .= "<option value='$key'>$value</option>";
        }
        $str .= "</select>";

        return $str;
    }

    /**
     * 回傳該地點的區域
     *
     * @param integer $locationId 地點的對應id
     * @return string
     */
    private function searchArea($locationId) {
        foreach ($this->areaMap as $key => $value) {
            if (in_array($locationId, $value)) {
                return $key;
            }
        }

        throw new \RuntimeException('取得區域錯誤');
    }
}
