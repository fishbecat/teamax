<?php

/**
 * 店面資料
 */
class MediaReport extends Base
{
    /**
     * 回傳店面資料
     *
     * @return array
     */
    public function show() {
        $query = $this->conn->query("SELECT * FROM media_report ORDER BY begin DESC");
        $result = $query->fetch_all(MYSQLI_ASSOC);

        return $result;
    }

    /**
     * 新增媒體報導資料
     *
     * @param array $options 包含內容如下
     *     begin integer 日期
     *     description string 描述
     *     uri string 網址
     *     filename string 圖檔名稱
     */
    public function insert($options) {
        $stmt = $this->conn->prepare(
            'INSERT INTO media_report (begin, description, uri, filename) VALUES (?, ?, ?, ?)'
        );

        $stmt->bind_param(
            'isss',
            $options['begin'],
            $options['description'],
            $options['uri'],
            $options['filename']
        );

        $stmt->execute();
    }

    /**
     * 修改店面資料
     *
     * @param array $options 包含內容如下
     *     id integer 媒體報導資料id
     *     begin integer 日期
     *     description string 描述
     *     uri string 網址
     *     filename string 圖檔名稱
     */
    public function edit($options) {
        $stmt = $this->conn->prepare(
            'UPDATE media_report SET begin = ?, description = ?, uri = ?, filename = ? WHERE id = ?'
        );

        $stmt->bind_param(
            'isssi',
            $options['begin'],
            $options['description'],
            $options['uri'],
            $options['filename'],
            $options['id']
        );

        $stmt->execute();
    }

    /**
     * 刪除店面資料
     *
     * @param integer $id 店面資料id
     */
    public function delete($id) {
        $stmt = $this->conn->prepare(
            'DELETE FROM media_report WHERE id = ?'
        );

        $stmt->bind_param('i', $id);
        $stmt->execute();
    }

    /**
     * 取得店面資料select bar
     *
     * @param string $date 日期
     * @return string
     */
    public function getBeginSelectBar($date = 'now') {
        $begin = new \DateTime($date);

        // 年
        $str = "<select class='concat' name='year'>";
        for ($i = 2001; $i <= date('Y') + 1; ++$i) {
            if ($i == $begin->format('Y')) {
                $str .= "<option value='$i' SELECTED>$i</option>";
                continue;
            }

            $str .= "<option value='$i'>$i</option>";
        }
        $str .= '</select>';

        // 月
        $str .= "<select class='concat' name='month'>";
        for ($i = 1; $i <= 12; ++$i) {
            if ($i == $begin->format('m')) {
                $str .= "<option value='$i' SELECTED>$i</option>";
                continue;
            }

            $str .= "<option value='$i'>$i</option>";
        }
        $str .= '</select>';

        // 日
        $str .= "<select class='concat' name='day'>";
        for ($i = 1; $i <= 31; ++$i) {
            if ($i == $begin->format('d')) {
                $str .= "<option value='$i' SELECTED>$i</option>";
                continue;
            }

            $str .= "<option value='$i'>$i</option>";
        }
        $str .= '</select>';

        return $str;
    }
}
