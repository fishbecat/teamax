<?php

/**
 * 最新消息
 */
class NewDrink extends Base
{
    /**
     * 回傳所有最新消息
     *
     * @return array
     */
    public function show() {
        $query = $this->conn->query('SELECT * FROM new_drink ORDER BY id DESC');

        return $query->fetch_all(MYSQLI_ASSOC);
    }

    /**
     * 新增最新消息
     *
     * @param array $options 包含內容如下
     *     title1 string 標題1
     *     description1 string 描述1
     *     title2 string 標題2
     *     description2 string 描述2
     *     filename string 圖檔名稱
     */
    public function insert($options) {
        $stmt = $this->conn->prepare(
            'INSERT INTO new_drink (at, title1, description1, title2, description2, filename) '.
            'VALUES (?, ?, ?, ?, ?, ?)'
        );

        $stmt->bind_param(
            'isssss',
            date('YmdHis'),
            $options['title1'],
            $options['description1'],
            $options['title2'],
            $options['description2'],
            $options['filename']
        );
        $stmt->execute();
    }

    /**
     * 修改最新消息
     *
     * @param array $options 包含內容如下
     *     title1 string 標題1
     *     description1 string 描述1
     *     title2 string 標題2
     *     description2 string 描述2
     */
    public function edit($options) {
        $stmt = $this->conn->prepare(
            'UPDATE new_drink SET title1 = ?, description1 = ?, title2 = ?, description2 = ? WHERE id = ?'
        );

        $stmt->bind_param(
            'ssssi',
            $options['title1'],
            $options['description1'],
            $options['title2'],
            $options['description2'],
            $options['id']
        );
        $stmt->execute();
    }

    /**
     * 刪除最新消息
     *
     * @param integer $id 最新消息id
     */
    public function delete($id) {
        $stmt = $this->conn->prepare(
            'DELETE FROM new_drink WHERE id = ?'
        );

        $stmt->bind_param('i', $id);
        $stmt->execute();
    }
}
