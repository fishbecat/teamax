<?php
$location = filter_input(INPUT_GET, 'location');

if (!$location) {
    $location = 7;
}

echo "<input type='hidden' name='location' value='$location'>";
?>

<div class="stores_select">
請選擇：<?php echo $stores->getLocationSelectBar($location, true); ?>
</div>

<fieldset>
<legend>新增資料</legend>
<div class="stores_insert">
    店名: <input type="text" name="name"><br>
    縣市: <?php echo $stores->getLocationSelectBar($location); ?><br>
    電話: <input type="text" name="telephone"><br>
    地址: <input type="text" name="address"><br>
    <input type="button" class="insert" value="新增"><br>
</div>
</fieldset>

<div class="stores_content">
<?php

$array = $stores->show($location);

if (!is_array($array)) {
    $json = json_decode($array, true);
    echo $json['msg'];
} else {
    foreach ($array as $value) {
        echo "<span class='left'>";
        echo "店名: <input type='text' name='name' value='$value[name]'><br>";
        echo "縣市: " . $stores->getLocationSelectBar($value['location']) . "<br>";
        echo "電話: <input type='text' name='telephone' value='$value[telephone]'><br>";
        echo "地址: <input type='text' name='address' value='$value[address]'><br>";
        echo "<input type='button' class='edit' value='修改'>&nbsp;";
        echo "<input type='button' class='delete' value='刪除'><br>";
        echo "<input type='hidden' name='id' value='$value[id]'>";
        echo "</span><br>";
    }
}
?>
</div>

<script>
var loc = $("input[name=location]").val();
var url = 'index.php?mode=stores';
var now = new Date();

$('.insert').on('click', function(event) {
    var target = $(event.target).parent();
    var form_data = new FormData();
    form_data.append('type', 'insert');
    form_data.append('name', target.find("input[name=name]").val());
    form_data.append('location', target.find("select[name=location]").val());
    form_data.append('address', target.find("input[name=address]").val());
    form_data.append('telephone', target.find("input[name=telephone]").val());

    var request = $.ajax({
        url: './modify_stores.php',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        method: 'post'
    });

    request.success(function(msg) {
        var data = JSON.parse(msg);
        if (data.result === 'error') {
            alert(data.msg);
        } else {
            window.location = url + '&location=' + loc + '&rd=' + now.getMilliseconds();
        }
    });

    request.error(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
});

$('.edit').click(function() {
    var target = $(event.target).parent();
    var form_data = new FormData();

    form_data.append('type', 'edit');
    form_data.append('id', target.find("input[name=id]").val());
    form_data.append('name', target.find("input[name=name]").val());
    form_data.append('location', target.find("select[name=location]").val());
    form_data.append('address', target.find("input[name=address]").val());
    form_data.append('telephone', target.find("input[name=telephone]").val());

    var request = $.ajax({
        url: './modify_stores.php',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        method: 'post'
    });

    request.success(function(msg) {
        var data = JSON.parse(msg);
        if (data.result === 'error') {
            alert(data.msg);
        } else {
            window.location = url + '&location=' + loc + '&rd=' + now.getMilliseconds();
        }
    });

    request.error(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
});

$('.delete').click(function() {
    var target = $(event.target).parent();
    var form_data = new FormData();

    form_data.append('type', 'delete');
    form_data.append('id', target.find("input[name=id]").val());

    var request = $.ajax({
        url: './modify_stores.php',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        method: 'post'
    });

    request.success(function(msg) {
        var data = JSON.parse(msg);
        if (data.result === 'error') {
            alert(data.msg);
        } else {
            window.location = url + '&location=' + loc + '&rd=' + now.getMilliseconds();
        }
    });

    request.error(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
});

$('#location_bar').change(function() {
    window.location = url + '&location=' + document.getElementById("location_bar").value + '&rd=' + now.getMilliseconds();
});
</script>