<fieldset>
<legend>新增資料</legend>
<div class="media_report_insert">
    日期: <?php echo $mediaReport->getBeginSelectBar(); ?><br>
    描述: <input type="text" name="description"><br>
    網址: <input type="text" name="uri"><br>
    圖片: <input type="file" name="file"><br>
    <input type="button" class="insert" value="新增"><br>
</div>
</fieldset>

<div class="media_report_content">
<?php

$array = $mediaReport->show();

foreach ($array as $value) {
    echo "<span class='left'>";
    echo "日期: " . $mediaReport->getBeginSelectBar($value['begin']) . "<br>";
    echo "描述: <input type='text' name='description' value='$value[description]'><br>";
    echo "網址: <input type='text' name='uri' value='$value[uri]'><br>";
    echo "圖片: <input type='file' name='file'><br>";
    echo "<input type='button' class='edit' value='修改'>&nbsp;";
    echo "<input type='button' class='delete' value='刪除'><br>";
    echo "<input type='hidden' name='id' value='$value[id]'>";
    echo "<input type='hidden' name='filename' value='$value[filename]'>";
    echo "</span>";

    echo "<span class='right'>";
    echo "<img src='../img/$value[filename]'>";
    echo "</span>";
}
?>
</div>

<script>
var url = 'index.php?mode=media_report';
var now = new Date();

$('.insert').on('click', function(event) {
    var target = $(event.target).parent();
    var form_data = new FormData();
    var year = target.find("select[name=year]").val() * 10000;
    var month = target.find("select[name=month]").val() * 100;
    var day = target.find("select[name=day]").val() * 1;

    form_data.append('type', 'insert');
    form_data.append('begin', year + month + day);
    form_data.append('description', target.find("input[name=description]").val());
    form_data.append('uri', target.find("input[name=uri]").val());
    form_data.append('file', target.find("input[name=file]").prop("files")[0]);

    var request = $.ajax({
        url: './modify_media_report.php',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        method: 'post'
    });

    request.success(function(msg) {
        var data = JSON.parse(msg);
        if (data.result === 'error') {
            alert(data.msg);
        } else {
            window.location = url + '&rd=' + now.getMilliseconds();
        }
    });

    request.error(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
});

$('.edit').click(function() {
    var target = $(event.target).parent();
    var form_data = new FormData();
    var year = target.find("select[name=year]").val() * 10000;
    var month = target.find("select[name=month]").val() * 100;
    var day = target.find("select[name=day]").val() * 1;

    form_data.append('type', 'edit');
    form_data.append('id', target.find("input[name=id]").val());
    form_data.append('begin', year + month + day);
    form_data.append('description', target.find("input[name=description]").val());
    form_data.append('uri', target.find("input[name=uri]").val());
    form_data.append('file', target.find("input[name=file]").prop("files")[0]);
    form_data.append('filename', target.find("input[name=filename]").val());

    var request = $.ajax({
        url: './modify_media_report.php',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        method: 'post'
    });

    request.success(function(msg) {
        var data = JSON.parse(msg);
        if (data.result === 'error') {
            alert(data.msg);
        } else {
            window.location = url + '&rd=' + now.getMilliseconds();
        }
    });

    request.error(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
});

$('.delete').click(function() {
    var target = $(event.target).parent();
    var form_data = new FormData();

    form_data.append('type', 'delete');
    form_data.append('id', target.find("input[name=id]").val());
    form_data.append('filename', target.find("input[name=filename]").val());

    var request = $.ajax({
        url: './modify_media_report.php',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        method: 'post'
    });

    request.success(function(msg) {
        var data = JSON.parse(msg);
        if (data.result === 'error') {
            alert(data.msg);
        } else {
            window.location = url + '&rd=' + now.getMilliseconds();
        }
    });

    request.error(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
});
</script>