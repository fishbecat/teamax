<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>喫茶享樂  Enjoy Tea</title>
<link href="css/main.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.11.3.min.js"></script>
</head>

<body>
    <div class="link">
        <a href="?mode=new_drink">新品消息</a>
        <a href="?mode=new_stores">新店消息</a>
        <a href="?mode=media_report">媒體報導</a>
        <a href="?mode=stores">門市資訊</a>
    </div>
    <br><hr><br>
    <div class="main">
    <?php
        require ('BackClass/Base.php');
        require ('BackClass/NewDrink.php');
        require ('BackClass/NewStores.php');
        require ('BackClass/MediaReport.php');
        require ('BackClass/Stores.php');

        $mode = filter_input(INPUT_GET, 'mode');

        if (!$mode || $mode == 'new_drink') {
            $newDrink = new NewDrink();

            include ('show_new_drink.php');
        }

        if ($mode == 'stores') {
            $stores = new Stores();

            include ('show_stores.php');
        }

        if ($mode == 'media_report') {
            $mediaReport = new MediaReport();

            include ('show_media_report.php');
        }

        if ($mode == 'new_stores') {
            $newStores = new NewStores();

            include ('show_new_stores.php');
        }
    ?>
    </div>
</body>
</html>
