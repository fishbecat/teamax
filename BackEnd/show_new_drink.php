<fieldset>
<legend>新增資料</legend>
<div class="new_drink_insert">
    標題1: <input type="text" name="title[]"><br>
    描述1: <input type="text" name="description[]"><br>
    標題2: <input type="text" name="title[]"><br>
    描述2: <input type="text" name="description[]"><br>
    圖片: <input type="file" name="file"><br>
    <input type="button" class="insert" value="新增"><br>
</div>
</fieldset>

<div class="new_drink_content">
<?php
$array = $newDrink->show();

foreach ($array as $value) {
    echo "<span class='left'>";
    echo "標題1: <input type='text' name='title[]' value='$value[title1]'><br>";
    echo "描述1: <input type='text' name='description[]' value='$value[description1]'><br>";
    echo "標題2: <input type='text' name='title[]' value='$value[title2]'><br>";
    echo "描述2: <input type='text' name='description[]' value='$value[description2]'><br>";
    echo "圖片: <input type='file' name='file'><br>";
    echo "<input type='button' class='edit' value='修改'>&nbsp;";
    echo "<input type='button' class='delete' value='刪除'><br>";
    echo "<input type='hidden' name='id' value='$value[id]'>";
    echo "<input type='hidden' name='filename' value='$value[filename]'>";
    echo "</span>";

    echo "<span class='right'>";
    echo "<img src='../img/$value[filename]'>";
    echo "</span>";
}
?>
</div>

<script>
var url = 'index.php?mode=new_drink';
var now = new Date();

$(".insert").on("click", function(event) {
    var target = $(event.target).parent();
    var form_data = new FormData();
    var title = {};

    form_data.append('type', 'insert');
    form_data.append(
        'title',
        JSON.stringify(target.find("input[name='title[]']").map(function(){return $(this).val();}).get())
    );
    form_data.append(
        'description',
        JSON.stringify(target.find("input[name='description[]']").map(function(){return $(this).val();}).get())
    );
    form_data.append('file', target.find("input[name=file]").prop("files")[0]);

    var request = $.ajax({
        url: './modify_new_drink.php',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        method: 'post'
    });

    request.success(function(msg) {
        var data = JSON.parse(msg);
        if (data.result === 'error') {
            alert(data.msg);
        } else {
            window.location = url + '&rd=' + now.getMilliseconds();
        }
    });

    request.error(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
});

$(".edit").click(function() {
    var target = $(event.target).parent();
    var form_data = new FormData();

    form_data.append('type', 'edit');
    form_data.append('id', target.find("input[name=id]").val());
    form_data.append(
        'title',
        JSON.stringify(target.find("input[name='title[]']").map(function(){return $(this).val();}).get())
    );
    form_data.append(
        'description',
        JSON.stringify(target.find("input[name='description[]']").map(function(){return $(this).val();}).get())
    );
    form_data.append('file', target.find("input[name=file]").prop("files")[0]);
    form_data.append('filename', target.find("input[name=filename]").val());

    var request = $.ajax({
        url: './modify_new_drink.php',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        method: 'post'
    });

    request.success(function(msg) {
        var data = JSON.parse(msg);
        if (data.result === 'error') {
            alert(data.msg);
        } else {
            window.location = url + '&rd=' + now.getMilliseconds();
        }
    });

    request.error(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
});

$(".delete").click(function() {
    var target = $(event.target).parent();
    var form_data = new FormData();

    form_data.append('type', 'delete');
    form_data.append('id', target.find("input[name=id]").val());
    form_data.append('filename', target.find("input[name=filename]").val());

    var request = $.ajax({
        url: './modify_new_drink.php',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        method: 'post'
    });

    request.success(function(msg) {
        var data = JSON.parse(msg);
        if (data.result === 'error') {
            alert(data.msg);
        } else {
            window.location = url + '&rd=' + now.getMilliseconds();
        }
    });

    request.error(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
});
</script>