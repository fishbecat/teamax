<?php

require ('BackClass/Base.php');
require ('BackClass/NewStores.php');

try {
    $newStores = new NewStores();

    // 取得會用到的參數
    $type = filter_input(INPUT_POST, 'type');
    $id = filter_input(INPUT_POST, 'id');
    $name = filter_input(INPUT_POST, 'name');
    $begin = filter_input(INPUT_POST, 'begin');
    $address = filter_input(INPUT_POST, 'address');
    $filename = filter_input(INPUT_POST, 'filename');

    if (!$type) {
        throw new \InvalidArgumentException('未指定類型');
    }

    // 上傳檔案失敗就丟例外
    if ($_FILES && $_FILES['file']['error'] != UPLOAD_ERR_OK) {
        throw new \RuntimeException('上傳檔案失敗');
    }

    // 如果有檔案需做處理
    if ($_FILES) {
        // 接受的檔案格式
        $legalFile = array(
            'jpg' => 'image/jpeg',
            'png' => 'image/png',
            'gif' => 'image/gif',
        );

        // 取得檔案格式，可找更嚴謹的方式
        $imgType = array_search($_FILES['file']['type'], $legalFile, true);

        if (!$imgType) {
            throw new \RuntimeException('不支援此檔案格式');
        }
    }

    // 新增
    if ($type == 'insert') {
        if (!$name) {
            throw new \InvalidArgumentException('未指定店名');
        }

        if (!$begin) {
            throw new \InvalidArgumentException('未指定創店日期');
        }

        if (!$address) {
            throw new \InvalidArgumentException('未指定地址');
        }

        if (!$_FILES) {
            throw new \InvalidArgumentException('未上傳圖片');
        }

        $filename = sprintf('%s.%s', md5(microtime()), $imgType);

        move_uploaded_file($_FILES['file']['tmp_name'], "../img/$filename");

        $options = array(
            'name' => $name,
            'begin' => $begin,
            'address' => $address,
            'filename' => $filename,
        );

        $newStores->insert($options);
    }

    // 修改
    if ($type == 'edit') {
        if (!$id) {
            throw new \InvalidArgumentException('未指定id');
        }

        if (!$begin) {
            throw new \InvalidArgumentException('未指定創店日期');
        }

        if (!$address) {
            throw new \InvalidArgumentException('未指定地址');
        }

        if ($_FILES) {
            move_uploaded_file($_FILES['file']['tmp_name'], "../img/$filename");
        }

        $options = array(
            'id' => $id,
            'name' => $name,
            'begin' => $begin,
            'address' => $address,
            'filename' => $filename,
        );

        $newStores->edit($options);
    }

    // 刪除
    if ($type == 'delete') {
        if (!$id) {
            throw new \InvalidArgumentException('未指定id');
        }

        if (!$filename) {
            throw new \InvalidArgumentException('未指定檔案名稱');
        }

        $newStores->delete($id, $filename);

        unlink("../img/$filename");
    }

    echo json_encode(array('result' => 'ok'));
} catch (\Exception $e) {
    $output = array(
        'result' => 'error',
        'msg' => $e->getMessage()
    );

    echo json_encode($output);
}
