<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>喫茶享樂  Enjoy Tea</title>
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<!--layerslider-->
<link href="js/layerslider/css/layerslider.css" rel="stylesheet" type="text/css">
<!--layersliderEnd-->
</head>

<body id="news">
<?php
    require ('FrontClass/Base.php');
    require ('FrontClass/News.php');
    $news = new News();
    $drinks = $news->getNewDrink();
    $stores = $news->getNewStores();
?>
<!--[if lte IE 8]>
<div class="chromeframe">您正在使用 <strong>非常古老</strong> 的瀏覽器！強烈的建議 <a href="http://browsehappy.com/" target="_blank">更新您的瀏覽器</a> ，來改善您的上網體驗。</div>
<![endif]-->
<div id="header">
  <div id="headerInner"> <a href="index.html"><img src="img/logo1.png" name="logo" width="301" height="94" id="logo" /></a>
  						 <img class="title" src="img/NEWS.png" width="157" height="116"  alt=""/>
    <ul id="nav">
      <li id="navAbout"><a href="about.html" id="about">關於喫茶</a> <span id="inAbout"><img class="btnLeaf" src="img/leaf.png" width="31" height="22"  alt=""/><a href="about.html">品牌精神</a><a href="tactics.html">經營策略</a></span> </li>
      <li id="navNews"><a href="news.php" id="news">活動消息</a> <span id="inNews"><img class="btnLeaf" src="img/leaf.png" width="31" height="22"  alt=""/><a href="news.php">最新消息</a><a href="report.php">推薦報導</a></span> </li>
      <li id="navProducts"><a href="hot_drinks.html" id="products">飲品介紹</a> <span id="inProducts"><img class="btnLeaf" src="img/leaf.png" width="31" height="22"  alt=""/><a href="hot_drinks.html">人氣飲品</a><a href="menu.html">MENU</a></span></li>
      <li><a href="stores.php" id="branches">喫茶享樂去</a></li>
      <li id="navJoin"><a href="joinus.html" id="join">加盟專區</a> <span id="inJoin"><img class="btnLeaf1" src="img/leaf.png" width="31" height="22"  alt=""/><a href="joinus.html">加盟訊息</a><a href="joinus_process.html">加盟流程</a><a href="condition.html">加盟條件</a></span></li>
    </ul>
  </div>
</div>
<div id="bannerIn">
  <div id="bannerInner">
  <!--Banner動畫-->
  <div id="layerslider" style="width: 960px; height: 270px; position:relative; z-index:1;">
   		 <div class="ls-slide" data-ls="slidedelay: 4000; transition2d: 2,7,9;">
         <img class="ls-l" style="position:absolute;top:21px;left:135px;" data-ls="offsetxin:50;durationin:750;easingin:easeOutBack;skewxin:-50;offsetxout:-50;" src="img/bannerNE_01.jpg" width="275" height="44" alt=""/>
         <img class="ls-l" style="position:absolute;top:70px;left:220px;" src="img/bannerNE_02.jpg" width="94" height="26" alt=""/>
         <img class="ls-l" style="position:absolute;top:95px;left:440px;" data-ls="offsetxin:0;durationin:2000;delayin:1500;easingin:easeOutElastic;rotatexin:-90;transformoriginin:50% top 0;offsetxout:-200;durationout:1000;" src="img/bannerNE_03.jpg" width="290" height="59" alt=""/>
         <img class="ls-l" style="position:absolute;top:150px;left:425px;" data-ls="offsetxin:0;durationin:2000;delayin:2000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% top 0;offsetxout:-400;" src="img/bannerNE_04.jpg" width="327" height="47" alt=""/>
		 <img class="ls-l" style="position:absolute;top:14px;left:735px;white-space: nowrap;" data-ls="offsetxin:0;durationin:3000;delayin:500;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;" src="img/bannerNE_Ch.png" alt=""/>
          <img class="ls-l" style="position:absolute;top:112px;left:131px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:-200;durationin:2000;delayin:1500;easingin:easeInOutQuad;rotatein:90;" src="img/bannerNE_Gr.png" width="93" height="95" alt="">
           <img class="ls-l" style="position:absolute;top:91px;left:340px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:-200;durationin:2000;delayin:2000;easingin:easeInOutQuad;rotatein:90;" src="img/bannerNE_Wi.png" width="77" height="78" alt="">
             <img class="ls-l" style="position:absolute;top:30px;left:670px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:-200;durationin:2000;delayin:1700;easingin:easeInOutQuad;rotatein:90;" src="img/bannerNE_Or.png" width="60" height="58" alt=""/>
	</div>
    <ul id="subnavNews">
      <li><a href="news.php" id="event">最新消息</a></li>
      <li><a href="report.php" id="report">推薦報導</a></li>
    </ul>
  </div><!--Banner動畫end-->
  </div>
</div>
<div id="content">
  <div id="contentInnerNews">
  <!--右上列 首頁及聯絡我們-->
    <ul id="sidebar">
      <li><a href="index.html">首頁</a></li>
      <li><a href="contact.html">聯絡我們</a></li>
    </ul><!--右上列 首頁及聯絡我們end-->
   <div id="newDrinks"><img src="img/newDrinks.png" width="184" height="18"  alt=""/>
   <a href="#" class="prev01"><img src="img/left_btn.png" width="30" height="30" alt=""></a>
   <a href="#" class="next01"><img src="img/right_btn.png" width="30" height="30" alt=""></a>
   </div>
    <div class="anyClass01">
      <ul class="news_item">
        <?php
            foreach ($drinks as $drink) {
                echo "<li>";
                echo "<div><img src='./img/{$drink['filename']}' width='250' height='150' alt=''/></div>";
                if ($drink['title1'] && $drink['description1']) {
                    echo "<span>{$drink['title1']}</span>{$drink['description1']}";
                }

                if ($drink['title2'] && $drink['description2']) {
                    echo "<br/><span>{$drink['title2']}</span>{$drink['description2']}";
                }

                echo "<br/></li>";
            }
        ?>
      </ul>
    </div>

    <div id="newStores"><img src="img/newStores.png" width="184" height="18"  alt=""/>
    <a href="#" class="prev02"><img src="img/left_btn.png" width="30" height="30" alt=""></a>
    <a href="#" class="next02"><img src="img/right_btn.png" width="30" height="30" alt=""></a>
    </div>

    <div class="anyClass02">
      <ul class="news_item">
        <?php
            foreach ($stores as $store) {
                echo "<li>";
                echo "<div><img src='./img/{$store['filename']}' width='250' height='150'  alt=''/></div>";
                echo "<span>{$store['begin']}{$store['name']}</span><br/>{$store['address']}";
                echo "</li>";
            }
        ?>
      </ul>
    </div>
  </div>
</div>
<div id="footer">
  <div id="footerInner">
    <a href="index.html"><img src="img/logo.png" name="logo" width="240" height="100" id="footerLogo" /></a>
    <ul>
      <li><a href="about.html">關於喫茶</a></li>
      <li><a href="news.php">活動消息</a></li>
      <li><a href="hot_drinks.html">飲品介紹</a></li>
      <li><a href="stores.php">分店資訊</a></li>
      <li><a href="joinus.html">加盟專區</a></li>
      <li><a href="contact.html">聯絡我們</a></li>
      <li><a href="https://www.facebook.com/pages/%E8%94%A1%E7%B1%83%E5%AD%90/788019037902736" target="_blank"><img src="img/FB.png" width="14" height="14"  alt=""></a></li>
    </ul>
    <p id="address">地址：台中市十甲路216號 ‧ 服務專線：04-22135886 ‧ 加盟專線：0989-111528 陳先生</p>
    <p id="copyright">© 2013 喫茶享樂 All Rights Reserved. Designed by <a href="http://fishsaut.com/" title="魚躍創意．品牌．設計．網頁" target="_blank">魚躍創意</a></p>
  </div>
</div>

<script src="js/jquery-1.10.2.min.js"></script>
<!--layerslider-->
<script src="js/layerslider/js/greensock.js"></script>
<script src="js/layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
<script src="js/layerslider/js/layerslider.transitions.js"></script>
<!--layersliderEnd-->
<script src="js/jcarousellite-1.0.1.js"></script>
<script>
$(document).ready(function(){
	<!--layerslider-->
   $('#layerslider').layerSlider({});
   	<!--layersliderEnd-->
  $("#navAbout").hover(function(){
    $("#inAbout").stop().fadeIn(500);
 },function(){
    $("#inAbout").hide();
 });
  $("#navNews").hover(function(){
    $("#inNews").stop().fadeIn(500);
 },function(){
    $("#inNews").hide();
 });
   $("#navProducts").hover(function(){
    $("#inProducts").stop().fadeIn(500);
 },function(){
    $("#inProducts").hide();
 });
    $("#navJoin").hover(function(){
    $("#inJoin").stop().fadeIn(500);
 },function(){
    $("#inJoin").hide();
 });
});
</script>

<script>
$(function() {
    $(".anyClass01").jCarouselLite({
        btnNext: ".next01",
        btnPrev: ".prev01"
    });
	$(".anyClass02").jCarouselLite({
        btnNext: ".next02",
        btnPrev: ".prev02"
    });


});
</script>

</body>
</html>
